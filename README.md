# GitHubTS

Project for iOS challenge.

App has 3 page which are -with proper flow- repositories, repository detail and user detail.

## Repositories
This page shows repositories which are public.

Public repositories listed in UITableView with custom cell. When client tap on cell, screen will navigate to [Repository Detail](#repoDetail). 

~~~~
Rest API URI: 
https://api.github.com/repositories
~~~~

![Repositories](images/repositories.png)

## Repository Detail <a name="repoDetail" />
This page shows detail of selected repository.

Owner's avatar shown at top of screen. Repository's details listed in UITableView with custom cell. When client tap on avatar, screen will navigate to [Owner Detail](#ownerDetail)

~~~~
Rest API URI: 
https://api.github.com/repos/$user/$repo
~~~~

![Repository Detail](images/repository_detail.png)

## Owner Detail <a name="ownerDetail" />
This page shows detail of selected user.

Owner's details shown at top of screen with flat design. Owner's repositories listed in UITableView with custom cell.

~~~~
Rest API URI: 

for User Detail 
https://api.github.com/users/$user

for User Repositories
https://api.github.com/users/$user/repos
~~~~

![Owner Detail](images/owner_detail.png)
