//
//  GithubTSAPI.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import UIKit

class GithubTSAPI {
    private init() {}
    static let shared = GithubTSAPI()
    
    func fetchRepositories(callback: @escaping ([Repository]?) -> Void) {
        let url = URL(string: "https://api.github.com/repositories")!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if data != nil {
                do {
                    let repos = try JSONDecoder().decode([Repository].self, from: data!)
                    callback(repos)
                } catch {
                    print(error)
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }.resume()
    }
    
    func fetchRepositoryDetails(userRepo: String, callback: @escaping ((Repository?) -> Void)) {
        let url = URL(string: "https://api.github.com/repos/\(userRepo)")!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if data != nil {
                do {
                    let repo = try JSONDecoder().decode(Repository.self, from: data!)
                    callback(repo)
                } catch {
                    print(error)
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }.resume()
    }
    
    func fetchUserDetails(user: String, callback: @escaping ((Owner?) -> Void)) {
        let url = URL(string: "https://api.github.com/users/\(user)")!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if data != nil {
                do {
                    let repo = try JSONDecoder().decode(Owner.self, from: data!)
                    callback(repo)
                } catch {
                    print(error)
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }.resume()
    }
    
    func fetchRepositoriesOfUser(user: String, callback: @escaping (([Repository]?) -> Void)) {
        let url = URL(string: "https://api.github.com/users/\(user)/repos")!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if data != nil {
                do {
                    let repo = try JSONDecoder().decode([Repository].self, from: data!)
                    callback(repo)
                } catch {
                    print(error)
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }.resume()
    }
    
    func fetchImage(url: URL, callback: @escaping ((UIImage?) -> Void)) {
        let req = URLRequest(url: url)
        
        if let data = URLCache.shared.cachedResponse(for: req)?.data, let image = UIImage(data: data) {
            callback(image)
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data, let response = response, let image = UIImage(data: data) {
                    let cached = CachedURLResponse(response: response, data: data)
                    URLCache.shared.storeCachedResponse(cached, for: req)
                    callback(image)
                } else {
                    callback(nil)
                }
            }.resume()
        }
    }
}
