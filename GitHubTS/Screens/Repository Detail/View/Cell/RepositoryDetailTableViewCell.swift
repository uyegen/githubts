//
//  RepositoryDetailTableViewCell.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import UIKit

class RepositoryDetailTableViewCell: UITableViewCell {
    
    static let identifier: String = String(describing: RepositoryDetailTableViewCell.self)
    static let nibName: String = identifier
    
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    var viewModel: RepositoryDetailTableCellViewModel? {
        didSet {
            lblValue.text = viewModel?.detail?.value
            lblInfo.text = viewModel?.detail?.info
        }
    }
    
    func initViewModel(detail: RepositoryDetailCell) {
        viewModel = RepositoryDetailTableCellViewModel(detail: detail)
    }
    
}
