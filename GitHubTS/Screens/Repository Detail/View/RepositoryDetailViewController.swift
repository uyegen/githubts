//
//  RepositoryDetailViewController.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import UIKit

class RepositoryDetailViewController: UIViewController, UITableViewDataSource {
    
    
    // MARK: IBOutlets & Variables
    
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var tvRepoInformations: UITableView!
    
    static let storyboardId: String = String(describing: RepositoryDetailViewController.self)
    
    lazy var viewModel = {
        return RepositoryDetailViewModel()
    }()
    var userRepo: String?
    
    
    // MARK: UIViewController
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Repository Details"
        
        initIVAvatar()
        initTvRepoInformations()
        initViewModel()
    }
    
    
    // MARK: RepositoryDetailViewController
    
    
    private func initIVAvatar() {
        ivAvatar.isUserInteractionEnabled = true
        ivAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(avatarTap)))
    }
    
    @objc private func avatarTap(_ recognizer: UITapGestureRecognizer) {
        guard let userDetail = self.storyboard?.instantiateViewController(withIdentifier: UserDetailViewController.storyboardId) as? UserDetailViewController, let user = viewModel.user else {
            return
        }
        
        userDetail.user = user
        
        navigationController?.pushViewController(userDetail, animated: true)
    }
    
    private func initViewModel() {
        viewModel.userRepo = userRepo
        viewModel.updateUI = { [weak self] in
            DispatchQueue.main.async {
                self?.tvRepoInformations.reloadData()
                
                if let url = self?.viewModel.getAvatarUrl() {
                    self?.ivAvatar.fetchImageOver(url: url)
                }
            }
        }
        showLoading(onView: self.view)
        viewModel.fetchRepositoryDetails { [weak self] in
            self?.removeLoading()
        }
    }
    
    private func initTvRepoInformations() {
        tvRepoInformations.rowHeight = 65
        tvRepoInformations.dataSource = self
        tvRepoInformations.register(UINib(nibName: RepositoryDetailTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: RepositoryDetailTableViewCell.identifier)
    }
    
    
    // MARK: UITableViewDataSource
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.detailInformation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryDetailTableViewCell.identifier, for: indexPath) as? RepositoryDetailTableViewCell else {
            return UITableViewCell()
        }
        
        cell.initViewModel(detail: viewModel.detailInformation[indexPath.item])
        
        return cell
    }
    
    
}
