//
//  RepositoryDetailViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

class RepositoryDetailViewModel {

    var updateUI: (() -> Void)?

    var detailInformation = [RepositoryDetailCell]()

    var userRepo: String?
    
    var repository: Repository? {
        didSet {
            guard let r = repository else {
                return
            }
            
            var informations = [RepositoryDetailCell]()
            informations.append(RepositoryDetailCell(value: r.name, info: "Repo Name"))
            informations.append(RepositoryDetailCell(value: r.owner.login, info: "Repo Owner Name"))
            informations.append(RepositoryDetailCell(value: "\(r.forksCount ?? 0)", info: "Fork Count"))
            informations.append(RepositoryDetailCell(value: r.language ?? "", info: "Language"))
            informations.append(RepositoryDetailCell(value: r.defaultBranch ?? "", info: "Default Branch Name"))
            
            detailInformation.removeAll()
            detailInformation.append(contentsOf: informations)
            updateUI?()
        }
    }
    
    var user: String? {
        get {
            return repository?.owner.login
        }
    }
    
    func fetchRepositoryDetails(completion: (() -> Void)?) {
        guard let userRepo = userRepo else {
            return
        }
        
        GithubTSAPI.shared.fetchRepositoryDetails(userRepo: userRepo) { repo in
            if repo != nil {
                self.repository = repo
            }
            
            completion?()
        }
    }
    
    func getAvatarUrl() -> URL? {
        guard let uri = repository?.owner.avatarUrl else {
            return nil
        }

        return URL(string: uri)
    }
}
