//
//  RepositoryDetailTableCellViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

class RepositoryDetailTableCellViewModel {
    var detail: RepositoryDetailCell?
    
    init(detail: RepositoryDetailCell) {
        self.detail = detail
    }
}
