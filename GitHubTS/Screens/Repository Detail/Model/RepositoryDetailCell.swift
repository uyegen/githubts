//
//  RepositoryDetailCell.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

struct RepositoryDetailCell {
    let value: String
    let info: String
}
