//
//  ViewController.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 10.03.2022.
//

import UIKit

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: IBOutlets & Variables
    

    @IBOutlet weak var tvRepositories: UITableView!
    
    private lazy var viewModel = {
        return RepositoriesViewModel()
    }()
    
    
    // MARK: UIViewController
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Repositories"
        initTvRepositories()
        initViewModel()
    }
    
    
    // MARK: RepositoriesViewController
    
    
    private func initTvRepositories() {
        tvRepositories.dataSource = self
        tvRepositories.delegate = self
        tvRepositories.register(UINib(nibName: RepositoryTableCell.nibName, bundle: nil), forCellReuseIdentifier: RepositoryTableCell.identifier)
    }
    
    private func initViewModel() {
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tvRepositories.reloadData()
            }
        }
        showLoading(onView: self.view)
        viewModel.fetchRepositories { [weak self] in
            self?.removeLoading()
        }
    }

    
    // MARK: UITableViewDataSource
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableCell.identifier, for: indexPath) as? RepositoryTableCell else {
            return UITableViewCell()
        }
        
        cell.initViewModel(repository: viewModel.repositories[indexPath.item])
        
        return cell
    }
    
    
    // MARK: UITableViewDelegate
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let repositoryDetail = self.storyboard?.instantiateViewController(withIdentifier: RepositoryDetailViewController.storyboardId) as? RepositoryDetailViewController else {
            return
        }
        
        repositoryDetail.userRepo = viewModel.repositories[indexPath.item].fullName
        
        navigationController?.pushViewController(repositoryDetail, animated: true)
    }


}
