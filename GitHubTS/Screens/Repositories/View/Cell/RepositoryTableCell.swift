//
//  RepositoryTableCell.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import UIKit

class RepositoryTableCell: UITableViewCell {
    
    static let identifier: String = String(describing: RepositoryTableCell.self)
    static let nibName: String = identifier

    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblRepoName: UILabel!
    @IBOutlet weak var lblOwnerUserName: UILabel!
    
    var viewModel: RepositoryTableCellViewModel? {
        didSet {
            lblRepoName.text = viewModel?.repository?.name
            lblOwnerUserName.text = viewModel?.repository?.owner.login
            if viewModel?.repository?.owner.avatarUrl != nil {
                if let url = URL(string: viewModel!.repository!.owner.avatarUrl) {
                    ivAvatar.fetchImageOver(url: url)
                }
            }
        }
    }
    
    func initViewModel(repository: Repository) {
        viewModel = RepositoryTableCellViewModel(repository: repository)
    }
    
}
