//
//  Repository.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

struct Repository: Codable {
    let name: String
    let fullName: String
    let owner: Owner
    let language: String?
    let forksCount: Int?
    let defaultBranch: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case fullName = "full_name"
        case owner = "owner"
        case language = "language"
        case forksCount = "forks_count"
        case defaultBranch = "default_branch"
    }
}
