//
//  RepositoryTableCellViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

class RepositoryTableCellViewModel {
    var repository: Repository?
    
    init(repository: Repository) {
        self.repository = repository
    }
}
