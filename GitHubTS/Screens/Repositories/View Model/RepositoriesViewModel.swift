//
//  RepositoriesViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import Foundation

class RepositoriesViewModel {
    
    var reloadTableView: (() -> Void)?
    
    var repositories = [Repository]() {
        didSet {
            reloadTableView?()
        }
    }
    
    func fetchRepositories(completion: (() -> Void)?) {
        GithubTSAPI.shared.fetchRepositories { repos in
            if repos != nil {
                self.repositories = repos!
            }
            
            completion?()
        }
    }
    
}
