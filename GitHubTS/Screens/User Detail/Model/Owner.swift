//
//  Owner.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 14.03.2022.
//

import Foundation

struct Owner: Codable {
    let login: String
    let avatarUrl: String
    let email: String?
    let publicRepos: Int?
    let followers: Int?
    let following: Int?
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
        case email
        case publicRepos = "public_repos"
        case followers
        case following
    }
}
