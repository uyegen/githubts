//
//  UserDetailViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 14.03.2022.
//

import Foundation

class UserDetailViewModel {
    
    var updateUI: (() -> Void)?
    var reloadTableView: (() -> Void)?

    var user: String?
    
    var owner: Owner? {
        didSet {
            guard let _ = owner else {
                return
            }
            
            updateUI?()
        }
    }
    
    var repositories = [Repository]() {
        didSet {
            reloadTableView?()
        }
    }
    
    func fetchOverAPI(complation: (() -> Void)?) {
        guard let user = user else {
            return
        }
        
        let complationGroup = DispatchGroup()
        
        fetchUserDetails(user: user, dispatchGroup: complationGroup)
        fetchRepositoriesOfUser(user: user, dispatchGroup: complationGroup)
        
        complationGroup.notify(queue: .main) {
            complation?()
        }
    }
    
    private func fetchUserDetails(user: String, dispatchGroup: DispatchGroup) {
        dispatchGroup.enter()
        GithubTSAPI.shared.fetchUserDetails(user: user) { owner in
            if owner != nil {
                self.owner = owner
            }
            dispatchGroup.leave()
        }
    }
    
    private func fetchRepositoriesOfUser(user: String, dispatchGroup: DispatchGroup) {
        dispatchGroup.enter()
        GithubTSAPI.shared.fetchRepositoriesOfUser(user: user) { repositories in
            if repositories != nil {
                self.repositories = repositories ?? []
            }
            dispatchGroup.leave()
        }
    }
    
    func getAvatarUrl() -> URL? {
        guard let uri = owner?.avatarUrl else {
            return nil
        }

        return URL(string: uri)
    }
    
}
