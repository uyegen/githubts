//
//  UserDetailRepositoryCellViewModel.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 14.03.2022.
//

import Foundation

class UserDetailRepositoryCellViewModel {
    var repository: Repository?
    
    init(repository: Repository) {
        self.repository = repository
    }
}
