//
//  UserDetailViewController.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 13.03.2022.
//

import UIKit

class UserDetailViewController: UIViewController, UITableViewDataSource {
    
    
    // MARK: IBOutlets & Variables
    
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPublicRepos: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var tvRepositories: UITableView!
    
    static let storyboardId: String = String(describing: UserDetailViewController.self)
    
    lazy var viewModel = {
       return UserDetailViewModel()
    }()
    var user: String?
    
    
    // MARK: UIViewController
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "User Details"
        
        initTvRepositories()
        initViewModel()
    }
    
    
    // MARK: UserDetailViewController
    
    
    private func initTvRepositories() {
        tvRepositories.dataSource = self
        tvRepositories.register(UINib(nibName: UserDetailRepositoryTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: UserDetailRepositoryTableViewCell.identifier)
    }
    
    private func initViewModel() {
        viewModel.user = user
        viewModel.updateUI = { [weak self] in
            DispatchQueue.main.async {
                self?.lblUserName.text = "User Name: \(self?.viewModel.owner?.login ?? "")"
                self?.lblEmail.text = "E-Mail: \(self?.viewModel.owner?.email ?? "")"
                self?.lblPublicRepos.text = "Public Repos: \(self?.viewModel.owner?.publicRepos ?? 0)"
                self?.lblFollowers.text = "Followers: \(self?.viewModel.owner?.followers ?? 0)"
                self?.lblFollowing.text = "Following: \(self?.viewModel.owner?.following ?? 0)"
                
                if let url = self?.viewModel.getAvatarUrl() {
                    self?.ivAvatar.fetchImageOver(url: url)
                }
            }
        }
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tvRepositories.reloadData()
            }
        }
        showLoading(onView: self.view)
        viewModel.fetchOverAPI { [weak self] in
            self?.removeLoading()
        }
    }
    
    
    // MARK: UITableViewDataSource
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserDetailRepositoryTableViewCell.identifier, for: indexPath) as? UserDetailRepositoryTableViewCell else {
            return UITableViewCell()
        }
        
        cell.initViewModel(repository: viewModel.repositories[indexPath.item])
        
        return cell
    }

}
