//
//  UserDetailRepositoryTableViewCell.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 14.03.2022.
//

import UIKit

class UserDetailRepositoryTableViewCell: UITableViewCell {
    
    static let identifier: String = String(describing: UserDetailRepositoryTableViewCell.self)
    static let nibName: String = identifier
    
    @IBOutlet weak var lblRepoName: UILabel!
    
    var viewModel: UserDetailRepositoryCellViewModel? {
        didSet {
            lblRepoName.text = viewModel?.repository?.name
        }
    }
    
    func initViewModel(repository: Repository) {
        viewModel = UserDetailRepositoryCellViewModel(repository: repository)
    }
    
}
