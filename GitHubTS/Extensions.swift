//
//  Extensions.swift
//  GitHubTS
//
//  Created by Utku Yeğen on 12.03.2022.
//

import UIKit


// MARK: UIImageView


extension UIImageView {
    func fetchImageOver(url: URL) {
        DispatchQueue.global().async { [weak self] in
            GithubTSAPI.shared.fetchImage(url: url) { image in
                DispatchQueue.main.async {
                    self?.image = image
                }
            }
        }
    }
}


// MARK: UIViewController


var extLoadingSpinner: UIView?

extension UIViewController {
    func showLoading(onView: UIView) {
        let v = UIView.init(frame: onView.bounds)
        v.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        var i: UIActivityIndicatorView?
        if #available(iOS 13, *) {
            i = UIActivityIndicatorView(style: .large)
        } else {
            i = UIActivityIndicatorView(style: .whiteLarge)
        }
        i!.startAnimating()
        i!.center = v.center
        
        v.addSubview(i!)
        
        DispatchQueue.main.async {
            v.addSubview(i!)
            onView.addSubview(v)
        }
        
        extLoadingSpinner = v
    }
    
    func removeLoading() {
        DispatchQueue.main.async {
            extLoadingSpinner?.removeFromSuperview()
            extLoadingSpinner = nil
        }
    }
}
